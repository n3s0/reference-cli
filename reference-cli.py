#!/usr/bin/python

"""
Title: Reference CLI
Author: Timothy (n3s0)
File: reference.py
Description: Application I will be using to provide references for various topics.
"""

import argparse
import textwrap
import sys
from src import core
from src import phonetic_alphabet_ref
from src import morse_code_ref

parser = argparse.ArgumentParser(
        prog='reference',
        description=textwrap.dedent('''\
                Provides modules related to various topics. Data is outputted in a 
                readable table format. For example, some work in call centers and 
                need to utilize the phonetic alphabet. Type the needed command and 
                you'll have what you need. All within a CLI interface.
                '''),
        epilog=textwrap.dedent('''\
                For bug reports or feature requests. Like adding new reference tables.
                Please submit an issue to the projects GitLab repository so it can be 
                reviewed. 

                https://gitlab.com/n3s0/reference
                '''))

parser.add_argument("-l", "--list", help="show available modules and exit",
        action="store_true")
parser.add_argument("-s", "--show", type=str, 
        help="run reference module the user specified and exit")
parser.add_argument("--info", help="provide more info on the application",
        action="store_true")

args = parser.parse_args()

if args.list:
    print(core.banner())
    core.ref_modules()
elif args.info:
    print("Comming soon")
elif args.show == "natopa":
    print(core.banner())
    phonetic_alphabet_ref.nato_pa()
elif args.show == "int-morse":
    print(core.banner())
    morse_code_ref.international_morse_code()
else:
    print(core.banner())
    parser.print_help()
