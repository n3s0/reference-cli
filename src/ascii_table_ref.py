#!/usr/bin/python

"""
Title: ASCII Table Module
Author: Timothy (n3s0)
File: src/modules/ascii_table_ref.py
Description: Provides reference for an ASCII table.
Credit:
    - http://www.asciitable.com/
"""

from prettytable import PrettyTable

def ascii_table():
    """ Print out an ASCII table."""
    
    ascii_table_columns = [["0","0x00","0000",,"NUL","(null)"],
            ["1","0x01","0001",,"SOH","Start of header"],
            ["2","0x02","0002",,"STX","Start of text"],
            ["3","0x03","0003",,"ETX", "End of text"],
            ["4","0x04","0004",,"EOT","End of transmission"],
            ["5","0x05","0005",,"ENQ","Enquiry"],
            ["6","0x06","0006",,"ACK","Acknowledge"],
            ["7","0x07","0007",,"BEL","Bell"],
            ["2","0x02","0002",,"STX","Start of text"],
            ["2","0x02","0002",,"STX","Start of text"],
            ["2","0x02","0002",,"STX","Start of text"],
            ["2","0x02","0002",,"STX","Start of text"],
            ["2","0x02","0002",,"STX","Start of text"],
            ["2","0x02","0002",,"STX","Start of text"],
            ["2","0x02","0002",,"STX","Start of text"],
            ["2","0x02","0002",,"STX","Start of text"],
            ]

    ascii_table = PrettyTable(["Dec.","Hex.","Oct.","HTML", 
        "Char.","Desc."])

    for column in ascii_table_columns:
        ascii_table.add_row(column)

    print("================== ASCII Table ===================")
    print("American Standard Code for Information Interchange\n")
    print("Credit for data goes to the following:")
    print("    - http://www.asciitable.com/")
    print("==================================================")
    print(ascii_table)
