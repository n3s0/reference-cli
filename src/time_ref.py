"""
file: time_ref.py
description: Military time conversion tables. Shows the conversion from 24 hour clock to 8 hour clock.
"""

from prettytable import PrettyTable

def military_conv():
    """ """
    mil_conv_columns = [["1:00 AM", "0100", "Zero One Hundred Hours"],
                        ["1:30 AM", "0130", "Zero One Thirty Hours"],
                        ["2:00 AM", "0200", "Zero Two Hundred Hours"],
                        ["2:30 AM", "0230", "Zero Two Thirty Hours"],
                        ["3:00 AM", "0300", "Zero Three Hundred Hours"],
                        ["3:30 AM", "0330", "Zero Three Thirty Hours"],
                        ["4:00 AM", "0400", "Zero Four Hundred Hours"],
                        ["4:30 AM", "0430", "Zero Four Thirty Hours"],
                        ["5:00 AM", "0500", "Zero Five Hundred Hours"],
                        ["5:30 AM", "0530", "Zero Five Thirty Hours"],
                        ["1:30 AM", "0130", "Zero One Thirty Hours"],
                        ["1:00 AM", "0100", "Zero One Hundred Hours"],
                        ["1:30 AM", "0130", "Zero One Thirty Hours"],
                        ["1:00 AM", "0100", "Zero One Hundred Hours"],
                        ["1:30 AM", "0130", "Zero One Thirty Hours"],
                        ["1:00 AM", "0100", "Zero One Hundred Hours"],
                        ["1:30 AM", "0130", "Zero One Thirty Hours"],
                        ]

    mil_conv_table = PrettyTable(["Standard", "Military", "Pronunciation"])

def 24hr_8hr_conv():
    """ """
