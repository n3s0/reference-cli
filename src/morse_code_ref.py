#!/usr/bin/python

"""
Title: International Morse Code Chart Module
Author: Timtohy (n3s0)
File: src/modules/morse_code_ref.py
Description: Provides a table reference for the International Morse Code Standard.
Credit:
    - Special thanks for initial reference from https://morsecode.world/international/morse2.html
Potential Improvements: (Things to think about.)
    - Code consolidation is a possibility.
    - Research accuracy of the current tables.
    - Possible that the positioning and formatting could improve.
    - Structure of the code can improve.
    - Variable names could make more sense.
"""

# Import libraries
from prettytable import PrettyTable

def international_morse_code():
    """ Print tables of the International Morse Code Standard."""
    # List of Morse Code letters for the Letters Table.
    morse_code_letters = [["A", ".-", "N", "-."],
                          ["B", "-...", "O", "---"],
                          ["C", "-.-.", "P", ".--."],
                          ["D", "-..", "Q", "--.-"],
                          ["E", ".", "R", ".-."],
                          ["F", "..-.", "S", "..."],
                          ["G", "--.", "T", "-"],
                          ["H", "....", "U", "..-"],
                          ["I", "..", "V", "...-"],
                          ["J", ".---", "W", ".--"],
                          ["K", "-.-", "X", "-..-"],
                          ["L", ".-..", "Y", "-.--"],
                          ["M", "--", "Z", "--.."]]

    # Headers and initial part of the Letters Table.
    letters_table = PrettyTable(["Letters - Part 1", "Morse - Part 1", "Letters - Part 2", "Morse - Part 2"])

    # Build the Letters table.
    for letter in morse_code_letters:
        letters_table.add_row(letter)

    # List of Morse Code digits.
    morse_code_digits =  [["0", "-----"],
                          ["1", ".----"],
                          ["2", "..---"],
                          ["3", "...--"],
                          ["4", "....-"],
                          ["5", "....."],
                          ["6", "-...."],
                          ["7", "--..."],
                          ["8", "---.."],
                          ["9", "----."]]

    # Headers and intial part of the Digits Table.
    digits_table = PrettyTable(["Digit", "Morse"])

    # Build the Digits Table.
    for digit in morse_code_digits:
        digits_table.add_row(digit)

    # List for Morse Code Punctuation table.
    morse_code_punctuations = [["& - Ampersand", ".-..."],
                               ["' - Apostraphe", ".----."],
                               ["@ - At Sign", ".--.-."],
                               [") - Bracket, close (parenthesis)", "-.--.-"],
                               ["( - Bracket, open (parenthesis)", "-.--."],
                               [": - Colon", "---..."],
                               [", - Comma", "--..--"],
                               ["= - Equels sign", "-...-"],
                               ["! - Exclamation mark", "-.-.--"],
                               [". - Full-stop (period)", ".-.-.-"],
                               ["- - Hyphen", "-....-"],
                               ["+ - Plus sign", ".-.-."],
                               ["\" - Quotation marks", ".-..-."],
                               ["? - Question mark (query)", "..--.."],
                               ["// - Slash", "-..-."]]

    # Headers and initial part of the Punctuation Table.
    punctuation_table = PrettyTable(["Punctuation Mark", "Morse"])

    # Build the Punctuation Table.
    for punctuation in morse_code_punctuations:
        punctuation_table.add_row(punctuation)

    # List for Morse Code Prosigns.
    prosigns = [["<AA> - New Line", ".-.-"],
                ["<AR> - End of message", ".-.-."],
                ["<AS> - Wait", ".-..."],
                ["<BK> - Break", "-...-.-"],
                ["<BT> - New paragraph (also =)", "-...-"],
                ["<CL> - Going off the air (\"clear\")", "-.-..-.."],
                ["<CT> - Start copying", "-.-.-"],
                ["<DO> - Change to wabun code", "-..---"],
                ["<KN> - Invite a specific station to transmit", "-.--."],
                ["<SK> - End of transmission", "...-.-"],
                ["<SN> - Understood (also VE)", "...-."],
                ["<SOS> - Distress message (Save Our Ship)", "...---..."]]

    # Headers and initial part of the Prosign Table.
    # Prosigns are combinations of two or three letters sent together with no space
    # in between. They are indicated with angled brackets.
    prosign_table = PrettyTable(["Prosign", "Morse"])

    # Build the Prosign Table.
    for prosign in prosigns:
        prosign_table.add_row(prosign)

    # List of other phrases for the Other Phrases Morse Code Table.
    other_phrases = [["Over", "K"],
                     ["Roger", "R"],
                     ["See you later", "CUL"],
                     ["Be seeing you", "BCNU"],
                     ["You're", "UR"],
                     ["Signal report", "RST"],
                     ["Best regards", "73"],
                     ["Love and kisses", "88"]]

    # Headers and initial part of the Other Phrases Table.
    op_table = PrettyTable(["Other Phrases", "Abbreviation"])

    # Build the Other Phrases Table.
    for phrase in other_phrases:
        op_table.add_row(phrase)

    # List of Q Codes for the Q Codes Table.
    q_codes = [["QSL", "I acknowledge receipt"],
               ["QSL?", "Do you acknowledge?"],
               ["QRX", "Wait"],
               ["QRX?", "Should I wait?"],
               ["QRV", "I am ready to copy"],
               ["QRV?", "Are you ready to copy?"],
               ["QRL", "The frequency is in use"],
               ["QRL?", "Is the frequency in use?"],
               ["QTH", "My location is..."],
               ["QTH", "What is your location?"]]

    # Headers and initial part for the Q Code Table.
    q_code_table = PrettyTable(["Q Code", "Meaning"])

    # Build the Q Code Table.
    for q_code in q_codes:
        q_code_table.add_row(q_code)

    print("Morse Code Letters")
    print(letters_table)
    print("\nMorse Code Digits")
    print(digits_table)
    print("\nMorse Code Punctuation")
    print(punctuation_table)
    print("\nMorse Code Prosigns")
    print(prosign_table)
    print("\nMorse Code Other Phrases")
    print(op_table)
    print("\nMorse Code Q Code Phrases")
    print(q_code_table)

